# hh-nfc

<h1>Mod 1 - Equipamento</h1>
<p><br /><strong>Liga&ccedil;&atilde;o do&nbsp;m&oacute;dulo RFID RC-522 a Placa Nano V3 ATmega328P USB Mini-B&nbsp;</strong></p>
<table style="width: 253px;">
<tbody>
<tr>
<td style="font-weight: 400; width: 114px;">&nbsp;M&oacute;dulo RFID&nbsp;</td>
<td style="font-weight: 400; width: 123px;">Arduino Nano</td>
</tr>
<tr>
<td style="font-weight: 400; width: 114px; text-align: center;">3.3V</td>
<td style="font-weight: 400; width: 123px; text-align: center;">3.3V</td>
</tr>
<tr>
<td style="font-weight: 400; width: 114px; text-align: center;">RST</td>
<td style="font-weight: 400; width: 123px; text-align: center;">Pin 9</td>
</tr>
<tr>
<td style="font-weight: 400; width: 114px; text-align: center;">GND</td>
<td style="font-weight: 400; width: 123px; text-align: center;">GND</td>
</tr>
<tr>
<td style="font-weight: 400; width: 114px; text-align: center;">NC</td>
<td style="font-weight: 400; width: 123px; text-align: center;">Pin 12</td>
</tr>
<tr>
<td style="font-weight: 400; width: 114px; text-align: center;">MISO</td>
<td style="font-weight: 400; width: 123px; text-align: center;">Pin 11</td>
</tr>
<tr>
<td style="font-weight: 400; width: 114px; text-align: center;">SDK</td>
<td style="font-weight: 400; width: 123px; text-align: center;">Pin 13</td>
</tr>
<tr>
<td style="font-weight: 400; width: 114px; text-align: center;">SDA</td>
<td style="font-weight: 400; width: 123px; text-align: center;">Pin 9</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<div>
<div><br /><a href="https://www.arduino.cc/en/Main/Software">Download</a> -&gt;&nbsp;Arduino Software Windows,&nbsp;Mac,&nbsp;Linux<br /><a href="https://create.arduino.cc/">Acessar ONLINE</a> -&gt;&nbsp;Arduino Software Online<br /><a href="https://www.arduinolibraries.info/libraries/mfrc522"><br />Download</a>&nbsp;-&gt; RFID&nbsp;RC522&nbsp;MFRC522&nbsp;Arduino&nbsp;Library<br /><br /><br /></div>
<div>&nbsp;</div>
</div>


![RFID-RC522](https://3.bp.blogspot.com/-LIRYnr98scs/WxPHATmSfdI/AAAAAAABIlA/UzwXyHHkqCQpFBX0bW4NZXvnm_ARMAEDQCPcBGAYYCw/s1600/arduino-rfid-rc522-esquema.png)


[![Maker Tutor](https://img.youtube.com/vi/TJJ_1LiDDrc/0.jpg)](https://www.youtube.com/watch?v=TJJ_1LiDDrc)